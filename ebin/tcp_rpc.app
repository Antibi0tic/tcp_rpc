%%%-------------------------------------------------------------------
%%% @author antibi0tic
%%% @copyright (C) 2016, <COMPANY>
%%% @doc RPC over TCP server
%%% @end
%%% Created : 16. Jul 2016 5:17 PM
%%%-------------------------------------------------------------------
{application, tcp_rpc, [
	{description, "RPC over TCP server"},
	{vsn, "0.1"},
	{modules, [tr_app, tr_sup, tr_server]},
	{registered, [tr_sup]},
	{applications, [kernel, stdlib]},
	{mod, {tr_app, []}},
	{env, []}
]}.